var backdrop = document.querySelector(".backdrop");
var selectPlanButtons = document.querySelectorAll(".plan button");
var modal = document.querySelector(".modal");
var modalNoButton = document.querySelector(".modal__action--negative");
var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");

for (var i = 0; i < selectPlanButtons.length; i++) {
  selectPlanButtons[i].addEventListener("click", function () {
    // modal.style.display = "block";
    // backdrop.style.display = "block";
    //modal.className = ".open";
    modal.classList.add("open");
    backdrop.classList.add("open");
  });
}

backdrop.addEventListener("click", function () {
  mobileNav.classList.remove("open");
  closeModal();
});

modalNoButton ? modalNoButton.addEventListener("click", closeModal) : null;

function closeModal() {
  //modal.style.display = "none";
  //backdrop.style.display = "none";
  modal ? modal.classList.remove("open") : null;
  backdrop.classList.remove("open");
}

toggleButton.addEventListener("click", function () {
  //mobileNav.style.display = "block";
  //backdrop.style.display = "block";
  mobileNav.classList.add("open");
  backdrop.classList.add("open");
});
